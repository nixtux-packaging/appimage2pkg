#!/bin/sh
# Author: mikhailnov

if [ ! -f "${HOME}/.local/share/appimagekit/ONLYOFFICEDesktopEditors_no_desktopintegration" ]; then
	if [ ! -d "${HOME}/.local/share/appimagekit/" ]; then
		mkdir -p "${HOME}/.local/share/appimagekit/"
	fi
	touch "${HOME}/.local/share/appimagekit/ONLYOFFICEDesktopEditors_no_desktopintegration"
fi

cd /opt/onlyoffice-desktopeditors-appimage
/opt/onlyoffice-desktopeditors-appimage/AppRun "$@"

