# appimage2pkg

## What it does
Repack [OnlyOffice](https://www.onlyoffice.com/ru/download-desktop.aspx) AppImage to RPM or DEB package. 

* FUSE is required to run AppImage
* FUSE is not required to run repacked package.

## Что делает этот скрипт
Перепаковываем AppImage [OnlyOffice](https://www.onlyoffice.com/ru/download-desktop.aspx)  в RPM или DEB пакет.

* Для запуска AppImage требуется FUSE
* FUSE не требуется для запуска перепакованного пакета
* Полученный пакет можно централизированно распространять через свой репозиторий

### Было
* DesktopEditors-x86_64.AppImage
### Стало
* скрипт-запускатор, который отключает создание desktop-файла в `$HOME` встроенными в AppImage средствами ([data/appimage-run.sh](https://gitlab.com/nixtux-packaging/appimage2pkg/blob/master/data/appimage-run.sh) 
* desktop-файл в `/usr/share/applications`
* запуск в любой системе, для запуска не нужно открывать дыру безопасности — FUSE.

## Required Settings
In the file `appimage2pkg.manifest` set all required variables (package name, version, where to download the AppImage from etc.).

Edit:

* appimage2pkg.manifest
* Makefile
* debian/control
* *.spec

## Необходимые настройки
В файле `appimage2pkg.manifest` задайте все необходимые параметры (имя, версия, релиз пакета, откуда скачивать AppImage и пр.).

Отредактируйте:

* appimage2pkg.manifest
* Makefile
* debian/control
* *.spec

## Usage
`bash appimage2pkg.sh deb|rpm|clean|extract|tarball`

* `rpm` — build RPM package
* `deb` — build DEB package
* `clean` — clean junk left from building
* `extract` — exctract AppImage
* `tarball` — create tarball from current directory

`Makefile` is not intended to use be used directly. It's called from `appimage2pkg.sh`.

Before building, check your `Makefile` by running:  
`make DESTDIR=./tmp install`

## Использование
`bash appimage2pkg.sh deb|rpm|clean|extract|tarball`

* `rpm` — собрать RPM пакет
* `deb` — собрать DEB пакет
* `clean` — очистить оставшийся от сборки мусор
* `extract` — извлечь AppImage
* `tarball` — создать тарболл из текущей директории

`Makefile` не предназначен для вызовы напрямую. Он вызывается из скрипта `appimage2pkg.sh`.

Прежде чем запускать сборку пакета, проверьте свой `Makefile` запуском:  
`make DESTDIR=./tmp install`

## Environment / Среда
This script was written and used on Ubuntu 18.04. RPMs were built for and used on ALT Linux, ROSA.

Этот скрипт был написан и использован на Ubuntu 18.04. RPM пакеты собирались для ALT Linux, ROSA.

`sudo apt install rpm devscripts`
