
#DESTDIR=./tmp
PREFIX = /
MAINSCRIPT = appimage2pkg.sh

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	bash $(MAINSCRIPT) extract
	
	# I don't know why install -d does not work without mkdir -p (!)
	mkdir -p $(DESTDIR)/$(PREFIX)
	mkdir -p $(DESTDIR)/$(PREFIX)/usr/bin
	mkdir -p $(DESTDIR)/$(PREFIX)/opt/onlyoffice-desktopeditors-appimage
	mkdir -p $(DESTDIR)/$(PREFIX)/usr/share/icons/hicolor/256x256/apps
	mkdir -p $(DESTDIR)/$(PREFIX)/usr/share/applications/
	
	install -d $(DESTDIR)/$(PREFIX)/usr/bin
	install -d $(DESTDIR)/$(PREFIX)/opt/onlyoffice-desktopeditors-appimage
	#mv ./squashfs-root/* $(DESTDIR)/$(PREFIX)/opt/onlyoffice-desktopeditors-appimage
	cp -r ./squashfs-root/* $(DESTDIR)/$(PREFIX)/opt/onlyoffice-desktopeditors-appimage
	chmod -R 0755 $(DESTDIR)/$(PREFIX)/opt/onlyoffice-desktopeditors-appimage/
	#rm -fvr ./squashfs-root
	install -d $(DESTDIR)/$(PREFIX)/usr/share/icons/hicolor/256x256/apps
	install -d $(DESTDIR)/$(PREFIX)/usr/share/applications
	#install -m0755 ./DesktopEditors-x86_64.AppImage $(DESTDIR)/$(PREFIX)/bin/onlyoffice-desktopeditors-appimage
	install -m0644 ./data/asc-de-256.png $(DESTDIR)/$(PREFIX)/usr/share/icons/hicolor/256x256/apps/onlyoffice-desktopeditors-appimage.png
	install -m0644 ./data/onlyoffice-desktopeditors-appimage.desktop $(DESTDIR)/$(PREFIX)/usr/share/applications/onlyoffice-desktopeditors-appimage.desktop
	install -m0755 ./data/appimage-run.sh $(DESTDIR)/$(PREFIX)/usr/bin/onlyoffice-desktopeditors-appimage-runner
	
uninstall:
	rm -fv $(PREFIX)/bin/onlyoffice-desktopeditors-appimage-runner
	rm -fvr $(PREFIX)/opt/onlyoffice-desktopeditors-appimage
	rm -fv /usr/share/icons/hicolor/256x256/apps/onlyoffice-desktopeditors-appimage.png
	rm -fv /usr/share/applications/onlyoffice-desktopeditors-appimage.desktop

clean:
	bash $(MAINSCRIPT) clean
	
 
