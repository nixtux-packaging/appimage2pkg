#!/bin/bash
# Repack AppImage to RPM/DEB package which does not require FUSE to run the application
# OnlyOffice DesktopEditors is an example; another application may be repacked
# Author: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>
# License: GPLv3

. /etc/os-release
if [ "$ID" = 'altlinux' ]
	then rpmbuild_dir="RPM"
	else rpmbuild_dir="rpmbuild"
fi

load_config(){
	dir_name="${PWD##*/}"
	# source all variables from file checkinstall.manifest
	. appimage2pkg.manifest || exit 1
	# allows to set RPM_BUILD_DIR in *.manifest or as environmental variable
	if [ -z "$RPM_BUILD_DIR" ]; then
		RPM_BUILD_DIR="${HOME}/${rpmbuild_dir}/BUILDROOT/${pkg_name}-${pkg_version}-${pkg_release}.${pkg_arch}"
	fi
	echo "RPM_BUILD_DIR: ${RPM_BUILD_DIR}"
}

clean(){
	echo "Cleaning..."
	rm -f "${dir_name}.spec"
	rm -f description-pak
	rm -fr squashfs-root
}

extract(){
	chmod +x "$pkg_source_file"
	echo "Exctracting AppImage to ./squashfs-root ..."
	./"${pkg_source_file}" --appimage-extract 2>/dev/null
	chmod -x "$pkg_source_file"
	# https://github.com/ONLYOFFICE/appimage-desktopeditors/issues/3#issuecomment-429636852
	rm -fv ./squashfs-root/usr/bin/libgmp.so* || true
}

tarball(){
# buggy and bad code
### TODO: study tar's manual
	mkdir "${pkg_name}-${pkg_version}"
	cp -r ./* "${pkg_name}-${pkg_version}" || true
	tar -cf "${pkg_name}-${pkg_version}.tar" "${pkg_name}-${pkg_version}"
	rm -fr "${pkg_name}-${pkg_version}"
	TARBALL="${pkg_name}-${pkg_version}.tar"
}

build_prepare(){
	if [ ! -f "$pkg_source_file" ]; then
		wget -O "$pkg_source_file" "$pkg_source_URL"
	fi

	if [ "$pkg_type" = 'rpm' ]
		then
			mkdir -p "${HOME}/${rpmbuild_dir}/SOURCES/"
			mkdir -p "${RPM_BUILD_DIR}"
	fi

}

build_rpm(){
	tarball
	mv -v ./"$TARBALL" "${HOME}/${rpmbuild_dir}/SOURCES/"
	make DESTDIR="${RPM_BUILD_DIR}" install ## на ubuntu не надо
	rpmbuild --buildroot "${RPM_BUILD_DIR}" -ba "${pkg_name}.spec"
}

build_deb(){
	dpkg-buildpackage
}

build_checkinstall(){
# depreceated
	echo "$pkg_description" > description-pak

	checkinstall \
		--type="${pkg_type}" \
		--install="no" \
		--fstrans="yes" \
		--pkgname="${pkg_name}" \
		--pkgversion="${pkg_version}" \
		--pkgrelease="${pkg_release}" \
		--pkglicense="${pkg_license}" \
		--arch="${pkg_arch}" \
		--pkggroup="${pkg_group}" \
		--nodoc \
		--requires="bash" \
		--backup="no" \
		-y 
}

load_config

case "$1" in
	"rpm" )
		pkg_type="rpm"
		#clean
		build_prepare
		build_rpm
		#clean
	;;
	"deb" )
		pkg_type="debian"
		clean
		build_prepare
		build_deb
		clean
	;;
	"clean" )
		clean
	;;
	"extract" ) 
		extract
	;;
	"tarball" )
		tarball
	;;
	* )
		echo "Use as: ./appimage2pkg.sh rpm|deb|clean|extract|tarball"
		exit 1
	;;
esac
