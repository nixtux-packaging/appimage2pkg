Summary: OnlyOffice Desktop Editors (repacked AppImage, blobs)
Group: Office 
Name: onlyoffice-desktopeditors-appimage 
Version: 5.3.3
Release: 1
License: AGPL
URL: https://www.onlyoffice.com/ru/download-desktop.aspx
Source: onlyoffice-desktopeditors-appimage-%{version}.tar

# disable automatic dependencies by ldd
AutoReqProv: no
# disable automatic dependencies from rpmlib
AutoReq: no

%description
OnlyOffice Desktop Editors
Repackaed AppImage

%prep

%setup

%build
%makeinstall_std

%files
# I hardcoded paths, e.g. used /usr/bin instead of %_bindir macro,
# because they are hardcoded in other parts of the code
/usr/bin/*
/usr/share/icons/*/*/*
/usr/share/applications/*
%dir /opt/onlyoffice*
/opt/onlyoffice*/*
